package filter;

import java.io.*;
import java.nio.file.attribute.FileOwnerAttributeView;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class Main {
   private static List<String> list=new ArrayList<>();


    public static void main(String[] args) throws IOException {
        Scanner scanner=new Scanner(System.in);
        System.out.println("Chose type of data: (STRING-s или INT-i)");
        String typeData= scanner.nextLine();

        System.out.println("Write name of first file for merging ");
        String fileNameFirst= scanner.next();
        System.out.println("Write name of second file for merging ");
        String fileNameSecond= scanner.next();

        System.out.println("Write name of final file");
        String finalFileName= scanner.next();
        scanner.close();
       //
        File file = new File("C:/Users/User/Desktop/JavaHW/cft/sortCFT/src/main/java/files/"+finalFileName+".txt");
        if (file.createNewFile()){
            System.out.println("File is created!");
        }
        else{
            System.out.println("File already exists.");
        }


        String path1="sortCFT/src/main/java/files/"+fileNameFirst+".txt";
        String path2="sortCFT/src/main/java/files/"+fileNameSecond+".txt";
        String path3="sortCFT/src/main/java/files/"+finalFileName+".txt";
      mergeFiles(path1,path2,file);
     sortList(path3);
     createNewFile(path3,file);


    }
  //  сортировка значение по алфавиту или по возрастанию
    public static void sortList(String path){
        try {
            BufferedReader bufferedReader=new BufferedReader(new FileReader(path));
            String str;
            while ((str=bufferedReader.readLine()) !=null){
                list.add(str);
            }
            bufferedReader.close();


        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        Collections.sort(list);


    }
    //слияние двух файлов txt И создание третьего файла
    public static String mergeFiles(String path1,String path2,File file){
        StringBuilder sb1=new StringBuilder();
        StringBuilder sb2=new StringBuilder();
        String finishFile;
        try {
            BufferedReader bufferedReader1=new BufferedReader(new FileReader(path1));
            BufferedReader bufferedReader2=new BufferedReader(new FileReader(path2));

            while (bufferedReader1.ready()) {
                sb1.append(bufferedReader1.readLine()+"\n");
            }
            while (bufferedReader2.ready()) {
                sb2.append(bufferedReader2.readLine()+"\n");
            }
            finishFile= String.valueOf(sb1.append(sb2));


            bufferedReader1.close();
            bufferedReader2.close();

        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        try (FileWriter fw = new FileWriter(file)) {

            fw.write(finishFile);
            System.out.println("Done!");

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return finishFile;
    }
   //запись данных
    public static void createNewFile(String path3,File file){

        try (FileWriter fw = new FileWriter(file)) {
            String data=list.toString();
            fw.write(data);
            System.out.println("Done!");

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    }
